from django import forms
from .models import Resume, Company
from qualities.models import TechnicalSkill, SoftSkill

class ResumeForm(forms.ModelForm):
    technical_skills = forms.ModelMultipleChoiceField(
        queryset=TechnicalSkill.objects.all().order_by('category'),
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )

    soft_skills = forms.ModelMultipleChoiceField(
        queryset=SoftSkill.objects.all().order_by('category'),
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )
    
    target_company = forms.ModelChoiceField(
        queryset=Company.objects.all().order_by('name'),
        widget=forms.CheckboxSelectMultiple,  # Change widget to CheckboxSelectMultiple
        required=False,
    )

    class Meta:
        model = Resume
        fields = ['name', 'description', 'summary', 'target_company', 'technical_skills', 'soft_skills']
